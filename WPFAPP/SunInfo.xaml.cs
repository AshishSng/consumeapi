﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFLibrary;

namespace WPFAPP
{
    /// <summary>
    /// Interaction logic for SunInfo.xaml
    /// </summary>
    public partial class SunInfo : Window
    {
        public SunInfo()
        {
            InitializeComponent();
            APIHelper.InitializeClient();
        }   

       

        private async void loadSunInfo_Click(object sender, RoutedEventArgs e)
        {
            var sunInfo = await SunProcessor.LoadSunInformation();
            cityText.Text = "City: Fargo";
            sunriseText.Text = $"Sunrise is at { sunInfo.Sunrise.ToLocalTime().ToShortTimeString() }";
            sunsetText.Text = $"Sunset is at {sunInfo.Sunset.ToLocalTime().ToShortTimeString() }";
            dayLengthText.Text = $"Length of day is {sunInfo.Day_Length.Hour} hours and {sunInfo.Day_Length.Minute} minutes.";
        }
    }
}
