This C#.WebAPI application using fallowing public api to show comic and SunInformation respectively.
	1. https://xkcd.com/json.html
	2. https://sunrise-sunset.org/api

This application showing xkcd current comic with Previous and Next buttons. when clicking on Previous and Next Buttons, shows previous and next comic. 

Center has one more button "Sun Information". Clicking on this button opens one more window "Sun Info". This window has only one button "Load Sun Info".
User will get city name, sunrise, sunset, and length of day info once user clicks on this button "Load Sun Info". (



References:-
This Project is inspired by a youtube channel IAmTimCorey ("https://www.youtube.com/watch?v=aWePkE2ReGw&t=2408s").

